import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PostListComponent } from './post-list/post-list.component';
import { IndividualPostComponent } from './individual-post/individual-post.component';
import { PagesComponent } from './pages.component';

const routes: Routes = [
  {
    path: 'posts', component: PagesComponent, children: [
      { path: '', component: PostListComponent },
      { path: ':postId', component: IndividualPostComponent },
    ]
  },
  { path: '', redirectTo: 'posts', pathMatch: 'full' },
  { path: '**', redirectTo: 'posts' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
