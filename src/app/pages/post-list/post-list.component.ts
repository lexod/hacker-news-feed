import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HackernewsService } from '../../@core/data/hackernews.service';
import { PostInterface } from '../../@core/interfaces/PostInterface';
import { Subscription } from 'rxjs';
import { TitleService } from '../../@core/shared/title.service';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit, OnDestroy {

  private subscription: Subscription = new Subscription();
  public posts: Array<PostInterface> = [];
  private _refreshFeedInterval;
  private _updateKarmaInterval;
  private _upperTimeBorder = null;
  private _lowerTimeBorder = null;
  private _page: number;
  private _perPage: number;

  constructor(private _router: Router,
              private _HNService: HackernewsService,
              private _titleService: TitleService) {
    this._page = 0;
    this._perPage = 18;

    this._titleService.updateTitle(' - Front Page');

    this.subscribeToUpdates();
    this.refreshFeed({ ...this.getDefaultRequestParams() });

    this._refreshFeedInterval = setInterval(() => {
      this.refreshFeed({ ...this.getDefaultRequestParams(), ...{ numericFilters: `created_at_i>${this._upperTimeBorder}` } });
    }, 20000);

    this._updateKarmaInterval = setInterval(() => {
      const param = this.getDefaultRequestParams();
      param.hitsPerPage = this.posts.length;
      this.refreshFeed({ ...param, ...{ numericFilters: `created_at_i<=${this._upperTimeBorder}` } });
    }, 15000);
  }

  private setPosts(posts: Array<any>, prepend: boolean = true): void {


    if (!this.posts.length || this.posts[0].objectID === posts[0].objectID) {
      console.log('Stats update triggered.');
      this.posts = posts;
      return;
    }

    if (prepend) {
      console.log('Feed update triggered.');
      this.posts = posts.concat(this.posts);
      return;
    }

    console.log('Pagination triggered.');
    this.posts = this.posts.concat(posts);
  }

  private subscribeToUpdates() {
    this.subscription.add(this._HNService.$dataStream.subscribe(response => {
      // skip if response is empty
      if (!response || !response.hits || !response.hits.length || Array.isArray(response)) {
        return;
      }
      // set a time border for live updates
      const newTimeBorder =
        response.hits.reduce((prev, current) => (prev.created_at_i > current.created_at_i) ? prev : current).created_at_i;
      this._lowerTimeBorder =
        response.hits.reduce((prev, current) => (prev.created_at_i < current.created_at_i) ? prev : current).created_at_i;
      // extract users to get karma
      let prepend = false;
      if (newTimeBorder > this._upperTimeBorder) {
        this._upperTimeBorder = newTimeBorder;
        prepend = true;
      }

      this.setPosts(response.hits, prepend);
    }));
  }

  getDefaultRequestParams() {
    return {
      tags: 'story',
      hitsPerPage: this._perPage,
    };
  }

  onScroll() {
    this.refreshFeed({ ...this.getDefaultRequestParams(), ...{ numericFilters: `created_at_i<${this._lowerTimeBorder}` } });
  }

  refreshFeed(params: object = {}) {
    this._HNService.getFeed(params);
  }

  navigateToPost(postId: number) {
    this._router.navigate([`posts/${postId}`]);
  }

  ngOnInit() {

  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    clearInterval(this._refreshFeedInterval);
    clearInterval(this._updateKarmaInterval);
  }

}
