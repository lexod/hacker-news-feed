import { Component, Input, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit {

  private _comment: any = {};
  public expanded = true;

  constructor() {
  }

  @Input()
  set comment(comment) {
    this._comment = comment;
  }

  get comment() {
    return this._comment;
  }

  toggleExpanded() {
    this.expanded = !this.expanded;
  }

  get formatTime(): string {
    const givenDate = moment(this.comment.created_at),
      unitsOfTime = ['years', 'months', 'weeks', 'days', 'hours', 'minutes'];

    for (let i = 0; i < unitsOfTime.length; i++) {
      const diff = moment.utc().diff(givenDate, <moment.unitOfTime.Diff>unitsOfTime[i]);
      if (diff) {
        return diff + ' ' + (diff === 1 ? unitsOfTime[i].substr(0, unitsOfTime[i].length - 1) : unitsOfTime[i]);
      }
    }

    return 'less then a minute';
  }

  ngOnInit() {
  }

}
