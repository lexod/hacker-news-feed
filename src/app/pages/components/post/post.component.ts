import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {

  private _post: any = {};

  constructor() {
    this.post = {};
  }

  @Input()
  set post(post) {
    this._post = post;
  }

  get post() {
    return this._post;
  }

  formatPoints() {
    return `${this.post.points} point${this.post.points !== 1 ? 's' : ''}`;
  }

  ngOnInit() {
  }

}
