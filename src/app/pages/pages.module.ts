import { NgModule } from '@angular/core';
import { PagesRoutingModule } from './pages-routing.module';
import { PostListComponent } from './post-list/post-list.component';
import { IndividualPostComponent } from './individual-post/individual-post.component';
import { PostComponent } from './components/post/post.component';
import { CommentComponent } from './components/comment/comment.component';
import { PagesComponent } from './pages.component';
import { HeaderComponent } from './components/header/header.component';
import { CommonModule } from '@angular/common';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

const PAGES_COMPONENTS = [
  PagesComponent,
  PostListComponent,
  IndividualPostComponent,
  PostComponent,
  CommentComponent,
  HeaderComponent,
];

@NgModule({
  imports: [
    CommonModule,
    PagesRoutingModule,
    InfiniteScrollModule,
  ],
  declarations: [...PAGES_COMPONENTS]
})
export class PagesModule {
}
