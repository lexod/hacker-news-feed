import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HackernewsService } from '../../@core/data/hackernews.service';
import { Subscription } from 'rxjs';
import { TitleService } from '../../@core/shared/title.service';

@Component({
  selector: 'app-individual-post',
  templateUrl: './individual-post.component.html',
  styleUrls: ['./individual-post.component.scss']
})
export class IndividualPostComponent implements OnInit, OnDestroy {

  private subscription: Subscription = new Subscription();
  public post;
  private _postId: number;
  private _refreshInterval;

  constructor(private _router: Router,
              private _route: ActivatedRoute,
              private _HNService: HackernewsService,
              private _titleService: TitleService) {
    this.subscribeToUpdates();

  }

  private subscribeToUpdates() {
    this.subscription.add(this._HNService.$itemStream.subscribe(response => {
      if (!response || Array.isArray(response)) {
        return;
      }

      if (!this.post) {
        this.post = response;
        return;
      }

      this.post.points = response.points;
    }));
    this.subscription.add(this._route.params.subscribe(params => {
      this.postId = params.postId;
      this._titleService.updateTitle(` - Post #${this.postId}`);
      this.refresh(this.postId);

      this._refreshInterval = setInterval(() => {
        this.refresh(this.postId);
      }, 15000);
    }));
  }

  set postId(postId: number) {
    this._postId = postId;
  }

  get postId() {
    return this._postId;
  }

  private refresh(postId: number) {
    this._HNService.getItem(this.postId);
  }

  ngOnInit() {
  }

  goToMain() {
    this._router.navigate(['posts']);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    clearInterval(this._refreshInterval);
  }
}
