import { ModuleWithProviders, NgModule } from '@angular/core';
import { TitleService } from './title.service';
import { CommonModule } from '@angular/common';

const SERVICES = [
  TitleService,
];

@NgModule({
  imports: [CommonModule],
  providers: [
    ...SERVICES,
  ],
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: SharedModule,
      providers: [
        ...SERVICES,
      ],
    };
  }
}
