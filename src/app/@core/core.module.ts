import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DataModule } from './data/data.module';
import { SharedModule } from './shared/shared.module';

export const CORE_PROVIDERS = [
  ...DataModule.forRoot().providers,
  ...SharedModule.forRoot().providers,
];

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: []
})
export class CoreModule {

  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: CoreModule,
      providers: [
        ...CORE_PROVIDERS,
      ],
    };
  }
}
