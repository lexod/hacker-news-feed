interface UserInterface {
  username: string;
  about?: string;
  karma: number;
}
