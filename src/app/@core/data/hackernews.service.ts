import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class HackernewsService {

  private _rootUrl: String = 'http://hn.algolia.com/api/v1';
  public $dataStream: BehaviorSubject<any> = new BehaviorSubject([]);
  public $itemStream: BehaviorSubject<any> = new BehaviorSubject([]);

  constructor(private _http: HttpClient) {
  }

  getFeed(params: object = {}) {
    return this._http.get(`${this._rootUrl}/search_by_date`, { params: this.prepareParam(params) })
      .subscribe(
        response => this.$dataStream.next(response),
        error => console.log(error)
      );
  }

  getItem(id: number) {
    return this._http.get(`${this._rootUrl}/items/${id}`)
      .subscribe(
        response => this.$itemStream.next(response),
        error => console.log(error)
      );
  }

  private prepareParam(data: object): HttpParams {
    let httpParams = new HttpParams();
    for (const key in data) {
      if (!data.hasOwnProperty(key)) {
        continue;
      }
      httpParams = httpParams.set(key, data[key]);
    }

    return httpParams;
  }
}
