# Hacker News Feed

This project runs on Angular 6.0.1.

## Table of contents
* [Requirements](#requirements)
* [Installation](#installation)
* [Development server](#development-server)
* [Code scaffolding](#code-scaffolding)
* [Build](#build)
* [Running unit tests](#running-unit-tests)
* [Running end-to-end tests](#running-end-to-end-tests)

## Requirements

* `NodeJS ^8.0.0`
* `Yarn` or `npm` 
* `nvm`(optional)

## Installation

* `nvm use`(optional - will set node version to v8.9.1)
* install dependencies with `yarn` or `npm`
* run a [dev server](#development-server) or a [build](#build)

## Development server

Run `yarn ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `yarn ng generate component component-name` to generate a new component. You can also use `yarn ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `yarn ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `yarn ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `yarn ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
